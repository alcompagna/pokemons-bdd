# Pokemons BDD

## Description

Ce projet a été réalisé dans le cadre de ma licence en binome avec Zakkaria BERRAHAL, un camarade. Il consitait à créer une application web qui permet à ses utilisateurs de collectionner, faire évoluer ou encore échanger des pokémons. Nous nous sommes pour cela appuyés sur JavaSpark, un framework Java. Vous pouvez retrouver plus de détails dans *Rapport.pdf* et vous pouvez tester vous même à l'aide de la commande **./gradlew run** et en allant sur localhost:8081.


## Auteurs

Ce projet à été réalisé en collaboration avec Zakkaria BERRAHAL.
