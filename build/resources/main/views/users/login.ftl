<#ftl encoding="utf-8">

<head>
      	<meta charset="UTF-8">
      	<title>Connexion</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
</head>
<style>
body {
  font-family: Arial, sans-serif;
  margin: 0;
  padding: 0;
}

header {
  background-color: #f0f0f0;
  padding: 20px;
  text-align: center;
}

h1 {
  margin: 0;
  font-size: 24px;
  font-weight: bold;
}

main {
  max-width: 400px;
  margin: 20px auto;
  padding: 20px;
  background-color: #f0f0f0;
  border-radius: 5px;
}

form {
  display: flex;
  flex-direction: column;
}

label {
  margin-bottom: 10px;
}

input {
  padding: 5px;
  border: 1px solid #ccc;
  border-radius: 3px;
}

button[type="submit"] {
  padding: 10px 20px;
  background-color: #4CAF50;
  color: white;
  border: none;
  border-radius: 3px;
  cursor: pointer;
}

button[type="submit"]:hover {
  background-color: #45a049;
}

footer {
  margin-top: 20px;
  text-align: center;
}

footer a {
  color: #555;
  text-decoration: none;
}

footer a:hover {
  text-decoration: underline;
}

</style>
<body>
	<header>
		<h1>CONNECTEZ VOUS</h1>
	</header>

	<main>
		<form id="login-form">

				<label>Pseudo
					<input type="text" class="name" id="name" name="name">
				</label>

				<label>Mot de passe
					<input type="password" class="password" id="password" name="password">
				</label>


				<button type="submit">Envoyer</button>
		</form>
		<footer>
			<a href="../">Accueil</a>
		</footer>
	</main>

	<script type="text/javascript">
	
	const form = document.getElementById('login-form');

	form.addEventListener('submit', (event) => {
	event.preventDefault();
	inscrireUtilisateur();
	});

	function inscrireUtilisateur() {
	const name = document.getElementById('name').value;
	const password = document.getElementById('password').value;

	fetch('/login', {
		method: 'POST',
		headers: {
		'Content-Type': 'application/json'
		},
		body: JSON.stringify({
		name,
		password
		})
	})
	.then(response => {
	if (response.ok) {
		// L'inscription a réussi
		console.log('Connexion réussie !');
		return response.json();
	} else {
		// L'inscription a échoué
		console.error('Une erreur est survenue lors de la connexion');
		throw new Error('La connexion a échoué');
	}
	})
	.then(data => {
		sessionStorage.setItem('token', data.token);
		console.log(`Votre jeton est : ${data.token}`);
		window.location.href = "/";
	})
	.catch(error => {
		console.error('Une erreur est survenue lors de la requête fetch', error);
	});
	}


	</script>
</body>
</html>