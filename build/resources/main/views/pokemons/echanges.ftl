<#ftl encoding="utf-8">
<style>
body {
  font-family: Arial, sans-serif;
  margin: 0;
  padding: 0;
}

table {
  width: 100%;
  border-collapse: collapse;
  margin: 20px;
}

th, td {
  padding: 10px;
  text-align: left;
}

th {
  background-color: #f0f0f0;
}

tr:nth-child(even) {
  background-color: #f9f9f9;
}

tr:hover {
  background-color: #e1e1e1;
}

button {
  padding: 5px 10px;
  background-color: #f0f0f0;
  border: none;
  border-radius: 5px;
  cursor: pointer;
}

header {
  margin: 20px;
  text-align: center;
}

header a {
  color: #555;
  text-decoration: none;
}

header a:hover {
  text-decoration: underline;
}

</style>
    <body>
        <header>
          <a href="../">Accueil</a>
        </header>
        <table>
            <tr>
                <th>Mon Pokémon</th>
                <th>Pokémon proposé</th>
            </tr>
            <#list echanges as echange>
                <tr>
                    <#list echange as pokemon>
                        <td><img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.idAPI}.png" alt="Image du Pokémon"> Niveau : ${pokemon.level}</td>
                    </#list>         
                    <td><button onclick="acceptExchange(${echange[0].id}, ${echange[1].id})">Accepter l'échange</button></td>
                </tr>
            </#list>
        </table>
        
    </body>

</html>
