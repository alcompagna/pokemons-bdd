<#ftl encoding="utf-8">

<head>
    <meta charset="UTF-8">
    <title>Accueil</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        h1 {
            text-align: center;
        }

        a {
            display: block;
            text-align: center;
            margin: 10px;
            padding: 10px 20px;
            background-color: #f0f0f0;
            color: #333;
            text-decoration: none;
            border-radius: 5px;
            transition: background-color 0.3s ease;
        }

        a:hover {
            background-color: #ccc;
        }
    </style>

    <h1>Accueil</h1>

    <a href="/register">Inscription</a>
    <a href="/login">Connexion</a>
    <a href="/users">Utilisateurs</a>
    <a href="/pokemonapi">Pokedex</a>
    <a href="/pokemons" onclick="addAuthorizationHeaderPokemons(event)">Pokemons</a>
    <a href="/profil" onclick="addAuthorizationHeader(event)">Profil</a>
    <a href="/echanges" onclick="addAuthorizationHeaderExchanges(event)">Echanges</a>



    <script>    
        function addAuthorizationHeaderExchanges(event) {
            event.preventDefault(); // Empêche le comportement par défaut du ligne

            var link = event.target;
            link.href = "/echanges";
            link.addEventListener("click", function() {
                var token = sessionStorage.getItem('token');
                if (token) {
                    fetch(link.href, {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + token
                        }
                    })
                    .then(response => {
                        if (response.ok) {
                            // La requête a réussi
                            return response.text(); // Renvoie le contenu de la réponse sous forme de texte HTML
                        } else {
                            // La requête a échoué
                            console.error('Une erreur est survenue lors de la requête');
                            throw new Error('La requête a échoué');
                        }
                    })
                    .then(html => {
                        // Afficher la page de profil
                        document.documentElement.innerHTML = html;
                    })
                    .catch(error => {
                        console.error('Une erreur est survenue lors de la requête fetch', error);
                    });
                } else {
                    console.error('Le token n\'existe pas');
                    window.location.href = "/login";

                }
            });
            link.click();
        }

        function addAuthorizationHeaderPokemons(event) {
            event.preventDefault(); // Empêche le comportement par défaut du lien

            var link = event.target;
            link.href = "/pokemons";
            link.addEventListener("click", function() {
                var token = sessionStorage.getItem('token');
                if (token) {
                    fetch(link.href, {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + token
                        }
                    })
                    .then(response => {
                        if (response.ok) {
                            // La requête a réussi
                            return response.text(); // Renvoie le contenu de la réponse sous forme de texte HTML
                        } else {
                            // La requête a échoué
                            console.error('Une erreur est survenue lors de la requête');
                            throw new Error('La requête a échoué');
                        }
                    })
                    .then(html => {
                        // Afficher la page de profil
                        document.documentElement.innerHTML = html;
                    })
                    .catch(error => {
                        console.error('Une erreur est survenue lors de la requête fetch', error);
                    });
                } else {
                    console.error('Le token n\'existe pas');
                    window.location.href = "/login";

                }
            });
            link.click();
        }
        
        function addAuthorizationHeader(event) {
            event.preventDefault(); // Empêche le comportement par défaut du lien

            var link = event.target;
            link.href = "/profil";
            link.addEventListener("click", function() {
                var token = sessionStorage.getItem('token');
                if (token) {
                    fetch(link.href, {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + token
                        }
                    })
                    .then(response => {
                        if (response.ok) {
                            // La requête a réussi
                            return response.text(); // Renvoie le contenu de la réponse sous forme de texte HTML
                        } else {
                            // La requête a échoué
                            console.error('Une erreur est survenue lors de la requête');
                            throw new Error('La requête a échoué');
                        }
                    })
                    .then(html => {
                        // Afficher la page de profil
                        document.documentElement.innerHTML = html;
                    })
                    .catch(error => {
                        console.error('Une erreur est survenue lors de la requête fetch', error);
                    });
                } else {
                    console.error('Le token n\'existe pas');
                    window.location.href = "/login";

                }
            });
            link.click();
        }

        function acceptExchange(idPok1, idPok2) {


            var data = {
                idPokemon1: idPok1,
                idPokemon2: idPok2
            };

            var token = sessionStorage.getItem('token');
            if (token) {
                fetch('/echange', {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    },
                    body: JSON.stringify(data)
                })
                .then(response => {
                    if (response.ok) {
                        return response.text();
                    } else {
                        throw new Error("Une erreur est survenue lors de la requête");
                    }
                })
                .then(html => {
                    document.documentElement.innerHTML = html;
                })
                .catch(error => {
                    console.error("Une erreur est survenue lors de la requête", error);
                });
            }
        }
        
        function askExchange(index) {
            //var ligne = document.getElementsByTagName("tr")[index + 1];
            //var idPokemon = ligne.cells[0].textContent;
            var idPokemon = index + 1;
            var data = {
                idPokemon: idPokemon
            };
            var token = sessionStorage.getItem('token');
            if (token) {
                fetch('/profilToExchange', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    },
                    body: JSON.stringify(data)
                })
                .then(response => {
                    if (response.ok) {
                        return response.text();
                    } else {
                        throw new Error("Une erreur est survenue lors de la requête");
                    }
                })
                .then(html => {
                    document.documentElement.innerHTML = html;
                })
                .catch(error => {
                    console.error("Une erreur est survenue lors de la requête", error);
                });
            }
        }

        function finalizeExchange(index, pokemon1) {
            
            var ligne = document.getElementById("index_"+index);
            var idPokemon1 = pokemon1;
            var idPokemon2 = ligne.innerHTML;
            var data = {
                idPokemon1: idPokemon1,
                idPokemon2: idPokemon2
            };
            
            var token = sessionStorage.getItem('token');
            if (token) {
                fetch('/createExchange', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    },
                    body: JSON.stringify(data)
                })
                .then(response => {
                    if (response.ok) {
                        return response.text();
                    } else {
                        throw new Error("Une erreur est survenue lors de la requête");
                    }
                })
                .then(html => {
                    document.documentElement.innerHTML = html;
                })
                .catch(error => {
                    console.error("Une erreur est survenue lors de la requête", error);
                });
            }
        }

        function lvlUpPokemon(index) {
            var ligne = document.getElementsByTagName("tr")[index + 1];
            var idPokemon = ligne.cells[0].textContent;
            var data = {
                idPokemon: idPokemon
            };
            var token = sessionStorage.getItem('token');
            if (token) {
                fetch('/lvlup', {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    },
                    body: JSON.stringify(data)
                })
                .then(response => {
                    if (response.ok) {
                        return response.text();
                    } else {
                        throw new Error("Une erreur est survenue lors de la requête");
                    }
                })
                .then(html => {
                    document.documentElement.innerHTML = html;
                })
                .catch(error => {
                    console.error("Une erreur est survenue lors de la requête", error);
                });
            }
        }

        function getDailyPokemon() {
            
            var token = sessionStorage.getItem('token');
            if (token) {
                fetch('/getpokemon', {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    },
                })
                .then(response => {
                    if (response.ok) {
                        return response.text();
                    } else {
                        throw new Error("Une erreur est survenue lors de la requête");
                    }
                })
                .then(html => {
                    document.documentElement.innerHTML = html;
                })
                .catch(error => {
                    console.error("Une erreur est survenue lors de la requête", error);
                });
            }
        }

    </script>

</body>
</html>
