<#ftl encoding="utf-8">

<style>
body {
  font-family: Arial, sans-serif;
  margin: 0;
  padding: 0;
}

ul {
  list-style-type: none;
  padding: 0;
  margin: 20px;
}

li {
  background-color: #f0f0f0;
  padding: 10px;
  margin-bottom: 10px;
  border-radius: 5px;
}

footer {
  margin-top: 20px;
  text-align: center;
}

footer a {
  color: #555;
  text-decoration: none;
}

footer a:hover {
  text-decoration: underline;
}

</style>
<body>

<ul>
    <#list users as user>
        <li>${user.id} - ${user.username}</li>
    </#list>
</ul>

<footer>
    <a href="../">Accueil</a>
</footer>

</body>

</html>
