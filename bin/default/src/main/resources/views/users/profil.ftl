<#ftl encoding="utf-8">
<head>
    <meta charset="UTF-8">
    <title>Profil</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<style>
body {
  font-family: Arial, sans-serif;
  margin: 0;
  padding: 0;
}

header {
  background-color: #f0f0f0;
  padding: 20px;
  text-align: center;
}

h1 {
  margin: 0;
}

main {
  margin: 20px;
}

button {
  padding: 10px 20px;
  background-color: #f0f0f0;
  border: none;
  border-radius: 5px;
  cursor: pointer;
}

table {
  width: 100%;
  border-collapse: collapse;
  margin-top: 20px;
}

th, td {
  padding: 10px;
  text-align: left;
}

th {
  background-color: #f0f0f0;
}

tr:nth-child(even) {
  background-color: #f9f9f9;
}

tr:hover {
  background-color: #e1e1e1;
}

footer {
  margin-top: 20px;
  text-align: center;
}

footer a {
  color: #555;
  text-decoration: none;
}

footer a:hover {
  text-decoration: underline;
}

</style>

<body>
    <header>
        <h1>BIENVENUE ${user.username}!</h1>
    </header>

    <main>
        <#if canPick>
            <button onclick="getDailyPokemon()">Obtenir un Pokémon</button>
        </#if>
        <table>
        <tr>
            <th>Id</th>
            <th>Image</th>
            <th>Nom</th>
            <th>Niveau</th>
            <th>Surnom</th>
        </tr>
            <#list pokemon as p>
                <tr>
                    <td id="index_${p_index}">${p.id}</td>
                    <td><img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${p.idAPI}.png" alt="Image du Pokémon"></td>
                    <td>${p.name}</td>
                    <td>${p.level}</td>
                    <td>${p.surname}</td>
                    <td><#if isExchanging> <button onclick="finalizeExchange(${p_index}, ${pExchange.id})">Échanger</button></#if></td>
                </tr>
            </#list>
            <#if isExchanging><li>Pokémon en cours d'échange : ${pExchange.id}</li></#if>
        </table>

        <footer>
            <a href="../">Accueil</a>
        </footer>
    </main>

    <script defer>
        document.getElementById("getPokemonButton").addEventListener("click", function() {
            var token = sessionStorage.getItem('token');
            fetch("/getpokemon", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': 'Bearer ' + token
                }
            })
            .then(response => {
                if (response.ok) {
                    return response.json(); // Traitez la réponse JSON
                } else {
                    throw new Error("Une erreur est survenue lors de la requête");
                }
            })
            .then(data => {
                console.log(data);
                // Faites ce que vous souhaitez avec les données
            })
            .catch(error => {
                console.error("Une erreur est survenue lors de la requête", error);
            });
        });
    </script>
</body>
</html>
