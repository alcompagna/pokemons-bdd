<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <title>Pokédex</title>
  <link rel="stylesheet" href="style.css">
</head>

<body>
    <style>
        .pokedex {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        }

        .pokemon-box {
        width: 200px;
        height: 200px;
        margin: 10px;
        padding: 10px;
        background-color: #f0f0f0;
        border-radius: 5px;
        text-align: center;
        }

        .pokemon-box img {
        width: 150px;
        height: 150px;
        margin-bottom: 10px;
        }

        .pokemon-box h3 {
        margin: 0;
        font-size: 16px;
        font-weight: bold;
        }

        .pokemon-box p {
        margin: 5px 0;
        }
    </style>
    <header>
        <a href="../">Accueil</a>
    </header>
    
    <div class="pokedex">
        <#list pokemonAPIs as pokemonAPI>
        <div class="pokemon-box">
            <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemonAPI.id}.png" alt="Image du Pokémon">
            <h3>${pokemonAPI.name}</h3>
        </div>
    </#list>
  </div>

</body>

</html>
