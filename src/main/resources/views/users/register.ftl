<#ftl encoding="utf-8">

<head>
    <meta charset="UTF-8">
    <title>Inscription</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
</head>
<style>
body {
  font-family: Arial, sans-serif;
  margin: 0;
  padding: 0;
}

header {
  background-color: #f0f0f0;
  padding: 20px;
  text-align: center;
}

h1 {
  margin: 0;
  font-size: 24px;
  font-weight: bold;
}

main {
  max-width: 400px;
  margin: 20px auto;
  padding: 20px;
  background-color: #f0f0f0;
  border-radius: 5px;
}

form {
  display: flex;
  flex-direction: column;
}

label {
  margin-bottom: 10px;
}

input {
  padding: 5px;
  border: 1px solid #ccc;
  border-radius: 3px;
}

button[type="submit"] {
  padding: 10px 20px;
  background-color: #4CAF50;
  color: white;
  border: none;
  border-radius: 3px;
  cursor: pointer;
}

button[type="submit"]:hover {
  background-color: #45a049;
}

footer {
  margin-top: 20px;
  text-align: center;
}

footer a {
  color: #555;
  text-decoration: none;
}

footer a:hover {
  text-decoration: underline;
}

</style>
<body>
    <header>
        <h1>INSCRIVEZ VOUS</h1>
    </header>

    <main>
        <section class="sectionInscription">
            <article>
                <form method="POST" action="/register" onsubmit="handleSubmit(event)">

                    <label>Pseudo
                        <input type="text" class="username" id="username" name="username">
                    </label>

                    <label>Mot de passe
                        <input type="password" class="password" id="password" name="password">
                    </label>


                    <button type="submit">Envoyer</button>
                </form>
            </article>
        </section>

        <footer>
          <a href="../">Accueil</a>
        </footer>
    </main>
    <script type="text/javascript">
    function handleSubmit(event) {
        event.preventDefault();
        let formData = {
            username: document.getElementById('username').value,
            password: document.getElementById('password').value,
        };

        let xhr = new XMLHttpRequest();
        xhr.open("POST", "http://localhost:8081/register");
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        //xhr.setRequestHeader("Authorization",sessionStorage.getItem("token"))
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log("success");
                window.location.href = "/";
            } else {
                console.log("error register");
            }
        };
        xhr.send(JSON.stringify(formData));
    }
    </script>

</body>

</html>