<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <title>Pokémons des Utilisateurs</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>

<style>
.pokemon-table {
  width: 100%;
  border-collapse: collapse;
}

.pokemon-table th,
.pokemon-table td {
  padding: 10px;
  text-align: center;
  border: 1px solid #ddd;
}

.pokemon-table th {
  background-color: #f2f2f2;
  font-weight: bold;
}

.pokemon-table tr:nth-child(even) {
  background-color: #f0f0f0;
}

.pokemon-action-button {
  padding: 5px 10px;
  background-color: #4CAF50;
  color: white;
  border: none;
  border-radius: 3px;
  cursor: pointer;
}

.pokemon-action-button:hover {
  background-color: #45a049;
}

header {
  margin: 20px;
  text-align: center;
}

header a {
  color: #555;
  text-decoration: none;
}

header a:hover {
  text-decoration: underline;
}

</style>
  <header>
    <a href="../">Accueil</a>
  </header>
  <table class="pokemon-table">
    <tr>
      <th>id</th>
      <th>Nom</th>
      <th>Image</th>
      <th>Niveau</th>
      <th>Propriétaire</th>
      <th>Id Propriétaire</th>
    </tr>
    <#list pokemons as pokemon>        
    <tr>
      <td id="pokemonId">${pokemon.id}</td>
      <td>${pokemon.name}</td>
      <td><img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.idAPI}.png" alt="Image du Pokémon"></td>
      <td>${pokemon.level}</td>
      <td>${pokemon.owner.username}</td>
      <td>${pokemon.ownerId}</td>
      <td><button class="pokemon-action-button" onclick="lvlUpPokemon(${pokemon_index})">Augmenter le niveau</button></td>
      <td><button class="pokemon-action-button" onclick="askExchange(${pokemon_index})">Demander l'échange</button></td>
    </tr>
    </#list>
  </table>

</body>

</html>
