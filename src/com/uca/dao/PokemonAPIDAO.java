package com.uca.dao;

import com.uca.entity.PokemonAPIEntity;
import com.uca.entity.PokemonEntity;

import java.sql.*;
import java.util.ArrayList;

public class PokemonAPIDAO {

    public Connection connect = _Connector.getInstance();

    public ArrayList<PokemonAPIEntity> getAllPokemonAPIs() {
        ArrayList<PokemonAPIEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM PokemonAPI ORDER BY id ASC;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PokemonAPIEntity entity = new PokemonAPIEntity();
                entity.setId(resultSet.getInt("id"));
                entity.setName(resultSet.getString("name"));

                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entities;
    }

    public PokemonEntity getPokemonById(int id) {
        PokemonEntity p = new PokemonEntity();
        try {
            // System.out.println(id);
            PreparedStatement statement;
            statement = this.connect.prepareStatement("SELECT name FROM pokemonAPI WHERE id=(?)");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            p.setSurname(resultSet.getString("name"));
            // System.out.println("test2");
            p.setIdAPI(id);
            // System.out.println("test3");
            p.setLevel(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return p;

    }

}
