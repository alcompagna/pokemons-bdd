package com.uca.dao;

import com.uca.entity.PokemonEntity;
import com.uca.entity.UserEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.time.LocalDate;
import java.time.ZoneId;

public class UserDAO extends _Generic<UserEntity> {

    public ArrayList<UserEntity> getAllUsers() {
        ArrayList<UserEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM users ORDER BY id ASC;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = new UserEntity();
                entity.setId(resultSet.getInt("id"));
                entity.setUsername(resultSet.getString("pseudo"));
                entity.setPassword(resultSet.getString("mdp"));

                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entities;
    }

    public void register(UserEntity user) {
        this.create(user);

    }

    public Boolean userExist(UserEntity user) {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM users;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if (user.getUsername().equals(resultSet.getString("pseudo"))
                        && user.getPassword().equals(resultSet.getString("mdp"))) {
                    return true;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // return true;
        return false;
    }

    public UserEntity getUserById(int id) {
        UserEntity user = new UserEntity();
        try {

            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM users WHERE id=?;");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            user.setId(id);
            user.setUsername(resultSet.getString("pseudo"));
            user.setPassword(resultSet.getString("mdp"));

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public int getIdByUser(UserEntity user) {
        int id = -1;
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM users WHERE pseudo=?;");

            preparedStatement.setString(1, user.getUsername());
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            id = resultSet.getInt("id");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public UserEntity create(UserEntity obj) {
        try {
            PreparedStatement preparedStatement = connect.prepareStatement(
                    "INSERT INTO users(pseudo,mdp, nb_lvlup, canPick) VALUES (?,?, ?, ?)");
            preparedStatement.setString(1, obj.getUsername());
            preparedStatement.setString(2, obj.getPassword());
            preparedStatement.setInt(3, 5);
            preparedStatement.setBoolean(4, true);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return obj;
    }

    @Override
    public void delete(UserEntity obj) {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "DELETE FROM users WHERE id =" + obj.getId());
            ResultSet resultSet = preparedStatement.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void lvlUp(UserEntity user, PokemonEntity pokemon) {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "SELECT * FROM users WHERE id =?");
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int nb_lvlup = resultSet.getInt("nb_lvlup");
            Date lastConnect = resultSet.getDate("lastConnect");
            // System.out.println(lastConnect);
            Date currentDate = new Date();

            // LocalDate localDateCurrent =
            // currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            // LocalDate localLastConnect =
            // lastConnect.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            // System.out.println("lastConnectLocal");
            // System.out.println("localDate ok");

            int yearComparison = Integer.compare(currentDate.getYear(), lastConnect.getYear());
            int monthComparison = Integer.compare(currentDate.getMonth(), lastConnect.getMonth());
            int dayComparison = Integer.compare(currentDate.getDay(), lastConnect.getDay());

            // System.out.println(yearComparison);
            // System.out.println(monthComparison);
            // System.out.println(dayComparison);

            if (yearComparison != 0 || monthComparison != 0 || dayComparison != 0) { // Si la derniere date de connexion
                                                                                     // n'est pas aujourd'hui
                nb_lvlup = 5;
            }
            // System.out.println("comparaison ok");

            // System.out.println(nb_lvlup);
            if (nb_lvlup > 0) {
                nb_lvlup -= 1;
                preparedStatement = this.connect.prepareStatement(
                        "UPDATE pokemon SET niveau = niveau + 1 WHERE id=?");
                preparedStatement.setInt(1, pokemon.getId());
                preparedStatement.executeUpdate();
                preparedStatement = this.connect.prepareStatement(
                        "UPDATE users SET nb_lvlup = nb_lvlup - 1 WHERE id=?");
                preparedStatement.setInt(1, user.getId());
                preparedStatement.executeUpdate();
                System.out.println(nb_lvlup);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void UpdateConnectDate(UserEntity user) {
        Date currentDate = new Date();
        System.out.println(currentDate);
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "UPDATE users SET lastConnect = ? WHERE id = ?");
            preparedStatement.setDate(1, new java.sql.Date(currentDate.getTime()));
            preparedStatement.setInt(2, user.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean canPick(UserEntity user) {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "SELECT canPick, lastConnect FROM users WHERE id=?");
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            boolean canPick = resultSet.getBoolean("canPick");
            Date lastConnect = resultSet.getDate("lastConnect");

            Date currentDate = new Date();

            if (canPick) {
                // preparedStatement = this.connect.prepareStatement(
                // "UPDATE users SET canPick=? WHERE id=?");
                // preparedStatement.setBoolean(1, false);
                // preparedStatement.setInt(2, user.getId());
                // preparedStatement.executeUpdate();
                return true;
            } else {
                int yearComparison = Integer.compare(currentDate.getYear(), lastConnect.getYear());
                int monthComparison = Integer.compare(currentDate.getMonth(), lastConnect.getMonth());
                int dayComparison = Integer.compare(currentDate.getDay(), lastConnect.getDay());

                if (yearComparison != 0 || monthComparison != 0 || dayComparison != 0) { // Si la derniere date de
                                                                                         // connexion n'est pas
                                                                                         // aujourd'hui

                    preparedStatement = this.connect.prepareStatement(
                            "UPDATE users SET canPick=? WHERE id=?");
                    preparedStatement.setBoolean(1, true);
                    preparedStatement.setInt(2, user.getId());
                    preparedStatement.executeUpdate();
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void hasPicked(UserEntity user) {
        Date currentDate = new Date();
        PreparedStatement preparedStatement;
        try {
            preparedStatement = this.connect.prepareStatement(
                    "UPDATE users SET canPick=? WHERE id=?");
            preparedStatement.setBoolean(1, false);
            preparedStatement.setInt(2, user.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
