package com.uca.dao;

import com.uca.entity.PokemonEntity;
import com.uca.entity.UserEntity;

import java.sql.*;
import java.util.ArrayList;

public class PokemonDAO extends _Generic<PokemonEntity> {

    public ArrayList<PokemonEntity> getAllPokemons() {
        ArrayList<PokemonEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM pokemon ORDER BY id ASC;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PokemonEntity entity = new PokemonEntity();
                entity.setId(resultSet.getInt("id"));
                entity.setIdAPI(resultSet.getInt("idAPI"));
                entity.setLevel(resultSet.getInt("niveau"));
                entity.setSurname(resultSet.getString("surnom"));
                entity.setOwnerId(resultSet.getInt("idProprietaire"));

                entities.add(entity);
            }

            int i = 0;
            preparedStatement = this.connect.prepareStatement(
                    "SELECT * FROM pokemon INNER JOIN users ON pokemon.idProprietaire = users.id ORDER BY pokemon.id ASC");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                UserEntity owner = new UserEntity();
                owner.setId(resultSet.getInt("id"));
                owner.setUsername(resultSet.getString("pseudo"));
                owner.setPassword(resultSet.getString("mdp"));

                entities.get(i).setOwner(owner);
                i++;
            }
            i = 0;
            preparedStatement = this.connect.prepareStatement(
                    "SELECT * FROM pokemonAPI INNER JOIN pokemon ON pokemon.idAPI = pokemonAPI.id ORDER BY pokemon.id ASC;");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                entities.get(i).setName(resultSet.getString("name"));
                i++;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entities;
    }

    public ArrayList<PokemonEntity> getAllPokemonsByUser(UserEntity user) {
        ArrayList<PokemonEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "SELECT * FROM pokemonAPI INNER JOIN pokemon ON pokemon.idAPI = pokemonAPI.id WHERE idProprietaire=(?) ORDER BY pokemon.id ASC;");
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PokemonEntity entity = new PokemonEntity();
                entity.setId(resultSet.getInt("pokemon.id"));
                entity.setIdAPI(resultSet.getInt("pokemon.idAPI"));
                entity.setLevel(resultSet.getInt("pokemon.niveau"));
                entity.setSurname(resultSet.getString("pokemon.surnom"));
                entity.setOwnerId(resultSet.getInt("pokemon.idProprietaire"));
                entity.setName(resultSet.getString("pokemonAPI.name"));

                entities.add(entity);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entities;
    }

    @Override
    public PokemonEntity create(PokemonEntity obj) {
        ResultSet resultSet = null;

        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "INSERT INTO pokemon(idAPI,niveau,surnom,idProprietaire) VALUES (?, ?, ?, ?);");
            // preparedStatement.setInt(1, obj.getId());
            preparedStatement.setInt(1, obj.getIdAPI());
            preparedStatement.setInt(2, obj.getLevel());
            preparedStatement.setString(3, obj.getSurname());
            preparedStatement.setInt(4, obj.getOwnerId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return obj;
    }

    @Override
    public void delete(PokemonEntity obj) {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "DELETE FROM users WHERE id =" + obj.getId());
            ResultSet resultSet = preparedStatement.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PokemonEntity getPokemonFromDB(int id) {

        PokemonEntity pokemon = new PokemonEntity();
        pokemon.setId(id);
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "SELECT * FROM pokemon WHERE pokemon.id=?;");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                pokemon.setIdAPI(resultSet.getInt("idAPI"));
                pokemon.setLevel(resultSet.getInt("niveau"));
                pokemon.setOwnerId(resultSet.getInt("idProprietaire"));
                pokemon.setLevel(resultSet.getInt("niveau"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pokemon;
    }

    public ArrayList<ArrayList<PokemonEntity>> getAllExchanges(UserEntity user) {
        ArrayList<ArrayList<PokemonEntity>> echanges = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "SELECT * FROM echanges INNER JOIN pokemon on pokemon.id = echanges.idPoke1 INNER JOIN users on users.id = pokemon.idProprietaire WHERE users.id=? AND echanges.etat = 'en cours';");
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ArrayList<PokemonEntity> echange = new ArrayList<>();

                PokemonEntity entity1 = new PokemonEntity();
                PokemonEntity entity2 = new PokemonEntity();

                entity1.setId(resultSet.getInt("idPoke1"));
                entity1.setIdAPI(resultSet.getInt("idAPI"));
                entity1.setLevel(resultSet.getInt("niveau"));
                entity2.setId(resultSet.getInt("idPoke2"));

                try {
                    PreparedStatement preparedStatement2 = this.connect.prepareStatement(
                            "SELECT * FROM pokemon WHERE id=?;");
                    preparedStatement2.setInt(1, entity2.getId());
                    ResultSet resultSet2 = preparedStatement2.executeQuery();
                    while (resultSet2.next()) {
                        entity2.setIdAPI(resultSet2.getInt("idAPI"));
                        entity2.setLevel(resultSet2.getInt("niveau"));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                echange.add(entity1);
                echange.add(entity2);
                echanges.add(echange);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return echanges;
    }

    public void acceptExchange(PokemonEntity pok1, PokemonEntity pok2, UserEntity user) {
        try {
            // On récupère l'idUser du second utilisateur
            int idUser2 = 0;

            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "SELECT idProprietaire from pokemon INNER JOIN echanges ON echanges.idPoke2 = pokemon.id WHERE echanges.idPoke2=? ");
            preparedStatement.setInt(1, pok2.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                idUser2 = resultSet.getInt("pokemon.idProprietaire");
            }

            // On change idPropriétaire pour les deux pokémons

            preparedStatement = this.connect.prepareStatement(
                    "UPDATE pokemon SET idProprietaire=? WHERE id=?");
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setInt(2, pok2.getId());
            preparedStatement.executeUpdate();

            preparedStatement = this.connect.prepareStatement(
                    "UPDATE pokemon SET idProprietaire=? WHERE id=?");
            preparedStatement.setInt(1, idUser2);
            preparedStatement.setInt(2, pok1.getId());
            preparedStatement.executeUpdate();

            preparedStatement = this.connect.prepareStatement(
                    "UPDATE echanges SET etat = 'fini' WHERE idPoke1 = ? AND idPoke2 = ?");
            preparedStatement.setInt(1, pok1.getId());
            preparedStatement.setInt(2, pok2.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createExchange(PokemonEntity pok1, PokemonEntity pok2) {
        try {
            int idAPIUser1;
            int idAPIUser2;

            if (pok1.getIdAPI() == 0) {
                PreparedStatement preparedStatement = this.connect.prepareStatement(
                        "SELECT idAPI FROM echanges INNER JOIN pokemon ON echanges.idPoke1 = pokemon.id WHERE pokemon.id=?;");
                preparedStatement.setInt(1, pok1.getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    idAPIUser1 = resultSet.getInt("idAPI");
                    pok1.setIdAPI(idAPIUser1);
                }
            }

            if (pok2.getIdAPI() == 0) {
                PreparedStatement preparedStatement = this.connect.prepareStatement(
                        "SELECT idAPI FROM echanges INNER JOIN pokemon ON echanges.idPoke2 = pokemon.id WHERE pokemon.id=?;");
                preparedStatement.setInt(1, pok2.getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    idAPIUser2 = resultSet.getInt("idAPI");
                    pok2.setIdAPI(idAPIUser2);
                }
            }

            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "INSERT INTO echanges (idPoke1, idPoke2, etat) VALUES (?,?,?);");
            preparedStatement.setInt(1, pok1.getId());
            preparedStatement.setInt(2, pok2.getId());
            preparedStatement.setString(3, "en cours");
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
