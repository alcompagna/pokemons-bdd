package com.uca.dao;

import java.sql.*;

import java.io.IOException;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.URL;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.io.InputStreamReader;
import com.uca.utilities.InputStreamOperations;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.uca.entity.PokemonAPIEntity;

public class _Initializer {

    public static void Init() {
        Connection connection = _Connector.getInstance();

        try {
            PreparedStatement statement;
            statement = connection.prepareStatement(
                    "DROP TABLE IF EXISTS echanges;");
            statement.executeUpdate();

            statement = connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS users (id int primary key auto_increment, pseudo varchar(25), mdp varchar(50), lastConnect Date, nb_lvlup int, canPick boolean);");
            statement.executeUpdate();
            statement = connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS pokemonAPI (id int primary key auto_increment, name varchar(25));");
            statement.executeUpdate();
            statement = connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS pokemon (id int primary key auto_increment, idAPI int, niveau int, surnom varchar(100), idProprietaire int, foreign key (idProprietaire) references users(id), foreign key (idAPI) references pokemonAPI(id));");
            statement.executeUpdate();
            statement = connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS echanges (idEchange int primary key auto_increment, idPoke1 int, idPoke2 int, etat varchar(10), foreign key (idPoke1) references pokemon(id), foreign key (idPoke2) references pokemon(id));");
            statement.executeUpdate();

            statement = connection.prepareStatement("SELECT COUNT(*) FROM pokemonAPI");
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int count = resultSet.getInt(1);

            if (count == 0) {
                URL url = new URL("https://pokeapi.co/api/v2/pokemon?limit=1008&offset=0");

                HttpURLConnection connec = (HttpURLConnection) url.openConnection();

                connec.setRequestProperty("accept", "application/json");

                InputStream responseStream = connec.getInputStream();

                String result = InputStreamOperations.InputStreamToString(responseStream);

                JSONObject jsonObject = new JSONObject(result);
                JSONArray array = jsonObject.getJSONArray("results");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    PokemonAPIEntity p = new PokemonAPIEntity();
                    p.setName(obj.getString("name"));

                    PreparedStatement preparedStatement = connection.prepareStatement(
                            "INSERT INTO pokemonAPI (name) VALUES (?)");
                    preparedStatement.setString(1, p.getName());
                    preparedStatement.executeUpdate();
                }
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            throw new RuntimeException("could not create database !");
        }

    }
}
