package com.uca.core;

import com.uca.dao.PokemonDAO;
import com.uca.dao.PokemonAPIDAO;
import com.uca.entity.PokemonEntity;
import com.uca.gui.PokemonGUI;
import com.uca.entity.UserEntity;
import com.uca.dao.UserDAO;
import java.io.IOException;

import java.util.ArrayList;

public class PokemonCore {

    public static String getAllPokemons() throws IOException {
        try {
            ArrayList<PokemonEntity> pokemons = new PokemonDAO().getAllPokemons();
            return new PokemonGUI().getAllPokemons(pokemons);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public PokemonEntity getPokemonFromDB(int id) {
        PokemonDAO pokemonDAO = new PokemonDAO();
        return pokemonDAO.getPokemonFromDB(id);
    }

    public PokemonEntity getPokemonById(int id, UserEntity user) {
        PokemonAPIDAO pokemonAPIDAO = new PokemonAPIDAO();
        PokemonEntity p = pokemonAPIDAO.getPokemonById(id);
        p.setOwnerId(user.getId());
        return p;

    }

    public void addPokemon(PokemonEntity p) {
        PokemonDAO pokemonDAO = new PokemonDAO();
        pokemonDAO.create(p);
    }

    public static String lvlUp(PokemonEntity pokemon, UserEntity user) {
        new UserDAO().lvlUp(user, pokemon);
        try {
            return PokemonCore.getAllPokemons();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getAllExchanges(UserEntity user) {
        ArrayList<ArrayList<PokemonEntity>> echanges = new PokemonDAO().getAllExchanges(user);
        try {
            return new PokemonGUI().getAllExchanges(echanges);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    public static String acceptExchange(PokemonEntity pok1, PokemonEntity pok2, UserEntity user) {
        new PokemonDAO().acceptExchange(pok1, pok2, user);
        try {
            return PokemonCore.getAllExchanges(user);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void createExchange(PokemonEntity pok1, PokemonEntity pok2) {
        new PokemonDAO().createExchange(pok1, pok2);
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}