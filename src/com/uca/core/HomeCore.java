package com.uca.core;

import com.uca.gui.HomeGUI;
import com.uca.entity.UserEntity;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;

public class HomeCore {

    public static String afficheHome() throws IOException, TemplateException{
        
        try{
            return new HomeGUI().afficheHome();
        }catch(IOException exception){
            exception.printStackTrace();
            return "";
        }
    }

    public static String afficheHome(UserEntity user) throws IOException, TemplateException{
        try{
            return HomeGUI.afficheHome(user);
        }catch(IOException exception){
            exception.printStackTrace();
            return "";
        }
    }

}