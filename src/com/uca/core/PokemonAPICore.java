package com.uca.core;

import com.uca.gui.PokemonAPIGUI;
import com.uca.dao.PokemonAPIDAO;
import com.uca.entity.PokemonAPIEntity;

import java.io.IOException;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.util.ArrayList;

public class PokemonAPICore {

    public static String getAllPokemonAPIs() throws IOException, TemplateException {
        PokemonAPIGUI pokemonAPIGUI = new PokemonAPIGUI();
        PokemonAPIDAO pokemonAPIDAO = new PokemonAPIDAO();

        return pokemonAPIGUI.affichePokemonAPIs(pokemonAPIDAO.getAllPokemonAPIs());
    }

}