package com.uca.core;

import com.uca.dao.UserDAO;
import com.uca.dao.PokemonDAO;
import com.uca.gui.UserGUI;
import com.uca.entity.UserEntity;
import com.uca.entity.PokemonEntity;

import java.io.IOException;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.util.ArrayList;

public class UserCore {

    public static String getAllUsers() throws IOException, TemplateException {
        UserDAO userDAO = new UserDAO();
        UserGUI userGUI = new UserGUI();

        return userGUI.afficheUsers(userDAO.getAllUsers());
    }

    public static String afficheRegister() throws IOException {

        try {
            return new UserGUI().afficheRegister();
        } catch (IOException exception) {
            return "";
        }
    }

    public static void register(UserEntity user) {
        new UserDAO().create(user);
    }

    public static String afficheLogin() throws IOException {

        try {
            return new UserGUI().afficheLogin();
        } catch (IOException exception) {
            return "";
        }
    }

    public static String afficheProfil(UserEntity user, boolean exchanges, PokemonEntity pokemon)
            throws IOException, TemplateException {

        ArrayList<PokemonEntity> p = new PokemonDAO().getAllPokemonsByUser(user);
        UserGUI userGUI = new UserGUI();
        return userGUI.afficheProfil(user, p, exchanges, pokemon);
    }

    public Boolean userExist(UserEntity user) {
        UserDAO userDAO = new UserDAO();

        return userDAO.userExist(user);
    }

    public static void UpdateConnectDate(UserEntity user) {
        new UserDAO().UpdateConnectDate(user);
    }

    public static boolean canPick(UserEntity user) {
        return new UserDAO().canPick(user);
    }

    public static void hasPicked(UserEntity user) {
        new UserDAO().hasPicked(user);
    }

}
