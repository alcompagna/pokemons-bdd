package com.uca.utilities;

import java.io.IOException;
import com.uca.entity.UserEntity;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import spark.Request;
import spark.Response;
import java.util.Date;
import java.util.UUID;
import java.time.Instant;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import com.uca.dao.UserDAO;

import java.sql.SQLException;




public class Token {
    public static String genToken(UserEntity user) {
        return Jwts.builder()
            .setId(UUID.randomUUID().toString())
            .claim("name", user.getUsername())
            .claim("id", user.getId())
            .setIssuedAt(new Date(Instant.now().toEpochMilli()))
            .setExpiration(new Date(Instant.now().toEpochMilli() + 1000 * 60 * 60 * 3))
            .signWith(Keys.hmacShaKeyFor("9iR5oFICvKofjewGZ5UAtRVTrzHLcQVVaq+k7gJf3/Q/2U+p+I5H/yQZ3pWj2s+e9YzU1mW2P7/1U+ewWW8Hw==".getBytes()), SignatureAlgorithm.HS512)
            .compact();
    }

    public static UserEntity parseToken(String token) throws IllegalArgumentException {
        Jws<Claims> jws;
        try {
            jws = Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor("9iR5oFICvKofjewGZ5UAtRVTrzHLcQVVaq+k7gJf3/Q/2U+p+I5H/yQZ3pWj2s+e9YzU1mW2P7/1U+ewWW8Hw==".getBytes()))
                .build()
                .parseClaimsJws(token);
        } catch (JwtException e) {
            throw new IllegalArgumentException("Invalid token given, could not parse correctly");
        }

        int id = jws.getBody().get("id", Integer.class);
        return new UserDAO().getUserById(id);

    }
}