package com.uca.gui;

import com.uca.core.PokemonAPICore;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import java.util.ArrayList;
import com.uca.entity.PokemonAPIEntity;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class PokemonAPIGUI {

    public static String affichePokemonAPIs(ArrayList<PokemonAPIEntity> entities) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("pokemonAPIs", entities);

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("pokemonAPI/pokedex.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();

    }

}