package com.uca.gui;

import com.uca.core.HomeCore;
import com.uca.entity.UserEntity;


import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class HomeGUI {

    public static String afficheHome() throws IOException, TemplateException{
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("isLoggedIn",false);

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("home/home.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    public static String afficheHome(UserEntity user) throws IOException, TemplateException{
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("isLoggedIn",true);

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("home/home.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }
    
}

