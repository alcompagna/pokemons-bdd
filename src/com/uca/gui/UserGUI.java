package com.uca.gui;

import com.uca.core.UserCore;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import java.util.ArrayList;
import com.uca.entity.UserEntity;
import com.uca.entity.PokemonEntity;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class UserGUI {

    public static String afficheUsers(ArrayList<UserEntity> entities) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("users", entities);

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("users/users.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();

    }

    public static String afficheRegister() throws IOException {

        Configuration configuration = _FreeMarkerInitializer.getContext();

        Template template = configuration.getTemplate("users/register.ftl");
        template.setOutputEncoding("UTF-8");
        return template.toString();
    }

    public static String afficheLogin() throws IOException {

        Configuration configuration = _FreeMarkerInitializer.getContext();

        Template template = configuration.getTemplate("users/login.ftl");
        template.setOutputEncoding("UTF-8");
        return template.toString();
    }

    public String afficheProfil(UserEntity user, ArrayList<PokemonEntity> p, boolean exchanges, PokemonEntity pExchange)
            throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();
        // System.out.println("Id Pokemon 1");
        // System.out.println(pExchange.getId()); // Erreur : ici id = 0 (impossible)
        // System.out.println(pExchange.getIdAPI());
        input.put("user", user);
        input.put("pokemon", p);
        input.put("pExchange", pExchange);
        input.put("canPick", UserCore.canPick(user));
        input.put("isExchanging", exchanges);

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("users/profil.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }
}
