package com.uca.gui;

import com.uca.core.PokemonCore;
import com.uca.gui._FreeMarkerInitializer;
import com.uca.entity.PokemonEntity;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

public class PokemonGUI {

    public String getAllPokemons(ArrayList<PokemonEntity> pokemons) throws IOException, TemplateException {

        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("pokemons", pokemons);

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("pokemons/pokemons.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    public String getAllExchanges(ArrayList<ArrayList<PokemonEntity>> echanges) throws IOException, TemplateException {

        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("echanges", echanges);

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("pokemons/echanges.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }
}
