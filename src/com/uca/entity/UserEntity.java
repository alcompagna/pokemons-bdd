package com.uca.entity;

import java.sql.Timestamp;

public class UserEntity {
    private String username;
    private String password;
    private int id;
    private boolean isLoggedIn;

    // public UserEntity(String username,String password, String email,int id) {
    //     this.usename=username;
    //     this.password=password;
    //     this.email=email;
    //     this.id=id;
    //     this.isLoggedIn=false;
    // }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public boolean isLoggedIn(){
        return this.isLoggedIn;
    }

}
