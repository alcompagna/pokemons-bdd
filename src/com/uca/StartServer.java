package com.uca;

import com.uca.dao._Initializer;
import com.uca.gui.*;
import com.uca.core.*;
import com.uca.entity.UserEntity;
import com.uca.entity.PokemonEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uca.utilities.Token;

import java.lang.reflect.GenericArrayType;
import java.time.Instant;
import com.uca.dao.UserDAO;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import javax.servlet.http.HttpServletRequest;

import java.util.Random;

import static spark.Spark.*;

public class StartServer {

    public static void main(String[] args) {
        // Configure Spark
        staticFiles.location("/static/");
        port(8081);

        _Initializer.Init();

        // Defining our routes

        get("/", (req, res) -> {

            // res.type("application/json");
            return HomeCore.afficheHome();
        });

        get("/register", (req, res) -> {
            return UserCore.afficheRegister();
        });

        post("/register", (req, res) -> {
            try {
                String body = req.body();
                ObjectMapper mapper = new ObjectMapper();
                UserEntity user = mapper.readValue(body, UserEntity.class);
                UserCore.register(user);
                res.status(200);
                res.type("application/json");
                return mapper.writeValueAsString(user);
            } catch (Exception e) {
                e.printStackTrace();
                res.status(500);
                return null;
            }

        });

        get("/login", (req, res) -> {
            return UserCore.afficheLogin();
        });

        // post("/login", (req, res) -> {
        // try{
        // String body = req.body();
        // ObjectMapper mapper = new ObjectMapper();
        // UserEntity user = mapper.readValue(body, UserEntity.class);
        // UserCore.login(user);
        // res.status(200);
        // res.type("application/json");
        // return mapper.writeValueAsString(user);
        // }catch(Exception e){
        // e.printStackTrace();
        // res.status(500);
        // return null;
        // }
        // });

        post("/login", (req, res) -> {
            try {

                JSONObject jsonReq = new JSONObject(req.body());
                String name = jsonReq.getString("name");
                String password = jsonReq.getString("password");

                UserDAO userDAO = new UserDAO();
                UserCore userCore = new UserCore();
                UserEntity user = new UserEntity();
                user.setUsername(name);
                user.setPassword(password);
                user.setId(userDAO.getIdByUser(user));

                if (userCore.userExist(user)) {

                    // Ajoute le token dans la réponse JSON
                    String token = new Token().genToken(user);
                    JSONObject jsonResponse = new JSONObject();
                    jsonResponse.put("token", token);
                    res.status(200);
                    res.type("application/json");
                    res.body(jsonResponse.toString());
                    UserCore.canPick(user);
                    UserCore.UpdateConnectDate(user);
                    return res.body();
                } else {
                    res.status(401);
                    return "utilisateur inexistant";
                }
            } catch (Exception e) {
                e.printStackTrace();
                res.status(500);
                return null;
            }
        });

        get("/users", (req, res) -> {
            return UserCore.getAllUsers();
        });

        get("/pokemonapi", (req, res) -> {
            return PokemonAPICore.getAllPokemonAPIs();
        });

        // get("/profil", (req, res) ->{
        // // String token = req.headers("token");
        // // System.out.println(token);
        // // if (token!=null){
        // // // UserEntity user = new Token().parseToken(token);
        // // // JSONObject responseJson = new JSONObject();
        // // // responseJson.put("token", token);
        // // // res.type("application/json");

        // // return "bienvenue";//HomeCore.afficheHome(user);
        // // }

        // // // res.type("application/json");
        // // return "veuillez vous connecter";

        // String authorizationHeader = req.headers("Authorization");
        // if (authorizationHeader != null && authorizationHeader.startsWith("Bearer "))
        // {
        // String token = authorizationHeader.substring(7); // Supprime "Bearer " du
        // début
        // // Vérifiez et traitez le token comme vous le souhaitez
        // // UserEntity user = new Token().parseToken(token);
        // // ...
        // return "bienvenue"; // Retournez la réponse souhaitée pour un utilisateur
        // connecté
        // }

        // return "veuillez vous connecter"; // Retournez la réponse souhaitée pour un
        // utilisateur non connecté
        // });

        get("/profil", (req, res) -> {
            String authorizationHeader = req.headers("Authorization");

            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer")) {
                String token = authorizationHeader.substring(7);

                UserEntity user = new Token().parseToken(token);

                // System.out.print(user.getUsername());
                // JSONObject jsonResponse = new JSONObject();
                // jsonResponse.put("message", "Bienvenue");
                // res.type("application/json");
                return UserCore.afficheProfil(user, false, null);
            }

            res.redirect("/login");
            return null;
        });

        post("/profilToExchange", (req, res) -> {
            String authorizationHeader = req.headers("Authorization");
            JSONObject jsonReq = new JSONObject(req.body());

            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer")) {
                String token = authorizationHeader.substring(7);

                UserEntity user = new Token().parseToken(token);

                int idPokemon = jsonReq.getInt("idPokemon");
                // System.out.println("Id Pokemon 1"); // Ici id OK
                // System.out.println(idPokemon);
                PokemonEntity pokemon = new PokemonCore().getPokemonFromDB(idPokemon);
                System.out.println(pokemon.getId());
                return UserCore.afficheProfil(user, true, pokemon);
            }

            res.redirect("/login");
            return null;
        });

        post("/createExchange", (req, res) -> {

            String authorizationHeader = req.headers("Authorization");
            JSONObject jsonReq = new JSONObject(req.body());
            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer")) {
                String token = authorizationHeader.substring(7);

                UserEntity user = new Token().parseToken(token);
                int idPokemon1 = jsonReq.getInt("idPokemon1");
                System.out.println("Avant");
                System.out.println(idPokemon1);
                System.out.println("Apres");
                PokemonEntity pokemon1 = new PokemonCore().getPokemonFromDB((idPokemon1));
                // pokemon1.setId(idPokemon1);

                String idPokemon2 = jsonReq.getString("idPokemon2");
                PokemonEntity pokemon2 = new PokemonCore().getPokemonFromDB(Integer.parseInt(idPokemon2));
                // pokemon2.setId(Integer.parseInt(idPokemon2));
                // System.out.println(pokemon1.getId());
                // System.out.println(pokemon1.getIdAPI());
                // System.out.println(pokemon2.getId());
                // System.out.println(pokemon1.getIdAPI());
                PokemonCore.createExchange(pokemon1, pokemon2);
                return UserCore.afficheProfil(user, false, null);
            }

            res.redirect("/login");
            return null;
        });

        get("/getpokemon", (req, res) -> {
            PokemonCore pokemonCore = new PokemonCore();
            UserCore userCore = new UserCore();

            String authorizationHeader = req.headers("Authorization");
            String token = authorizationHeader.substring(7);
            UserEntity user = new Token().parseToken(token);

            Random random = new Random();
            int id = random.nextInt(1008);
            id++;

            PokemonEntity p = pokemonCore.getPokemonById(id, user);

            pokemonCore.addPokemon(p);
            // userCore.canPick(user);
            userCore.hasPicked(user);

            return new UserCore().afficheProfil(user, false, null);

        });

        get("/pokemons", (req, res) -> {
            return PokemonCore.getAllPokemons();
        });

        put("/lvlup", (req, res) -> {
            String authorizationHeader = req.headers("Authorization");

            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer")) {

                String token = authorizationHeader.substring(7);

                UserEntity user = new Token().parseToken(token);

                JSONObject jsonReq = new JSONObject(req.body());
                // System.out.println(jsonReq);

                String idPokemon = jsonReq.getString("idPokemon");
                // System.out.println(idPokemon);

                PokemonEntity pokemon = new PokemonEntity();
                pokemon.setId(Integer.parseInt(idPokemon));
                return PokemonCore.lvlUp(pokemon, user);
            } else {
                res.redirect("/login");
                return null;
            }
        });

        get("/echanges", (req, res) -> {
            String authorizationHeader = req.headers("Authorization");

            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer")) {

                String token = authorizationHeader.substring(7);

                UserEntity user = new Token().parseToken(token);

                return PokemonCore.getAllExchanges(user);
            } else {
                res.redirect("/login");
                return null;
            }

        });

        put("/echange", (req, res) -> {

            String authorizationHeader = req.headers("Authorization");
            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer")) {
                String token = authorizationHeader.substring(7);

                UserEntity user = new Token().parseToken(token);

                JSONObject jsonReq = new JSONObject(req.body());

                int idPokemon1 = jsonReq.getInt("idPokemon1");
                int idPokemon2 = jsonReq.getInt("idPokemon2");

                PokemonEntity pokemon1 = new PokemonCore().getPokemonFromDB(idPokemon1);
                // System.out.println(idPokemon1);
                // pokemon1.setId(Integer.parseInt(idPokemon1)); // Ici on parse une string vide
                // donc erreur

                PokemonEntity pokemon2 = new PokemonCore().getPokemonFromDB(idPokemon2);

                // pokemon2.setId(Integer.parseInt(idPokemon2));

                return PokemonCore.acceptExchange(pokemon1, pokemon2, user);
            } else {
                res.redirect("/login");
                return null;
            }
        });
    }
}